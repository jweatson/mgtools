#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 15:04:08 2017

@author: joseph
"""

# Modules for mgplot, allows for writing out of plots from .xq exports
import numpy as np
import numpy as np # 
from numba import jit #Numba for jit compilation for faster math with transforms
import matplotlib.pyplot as plt #Plotting architecture, runs slowly, suggest using batch output
import multiprocessing as mult #Multiprocessing for batch commands

reslaberr="Could not convert header correctly\nPlease confirm header is same as stock mg export!"

def scale(array,scale):
    maniparray=np.multiply(array,scale)
    return maniparray
        
def readdata(filename):
    """
    Function reads data from filename, converting into a numpy array
    Generates a header for the file, consisting of dimensions and units in a dictionary
    Conversion takes a long time if working in 3D, and also consumes a lot of memory
    """
    def readheader(filename):
        header={}
        f=open(filename)
        l1=f.next().strip("\n").split(" ")
        f.next()
        l2=f.next().strip("\n").split(" ")
        header["ndim"]=int(l1[0])
        if header["ndim"]>0:
            header["xres"]=l1[1]
            try: header["xres"]=int(header["xres"])
            except: print reslaberr
            header["xlab"]=l2[0]
        if header["ndim"]>1:
            header["yres"]=l1[2]
            header["ylab"]=l2[1]
            try: header["yres"]=int(header["yres"])
            except: print reslaberr
        if header["ndim"]>2:
            header["zres"]=l1[3]
            header["zlab"]=l2[2]
            try: header["zres"]=int(header["zres"])
            except: print reslaberr
        return header
    header=readheader(filename)
    data=np.genfromtxt(filename,dtype=float,skip_header=3)
    return data,header

def mucalc(x=1,y=0,z=0):
    """
    Calculates mu, the mean molecular weight, of a flow
    Requires 3 variables, x, y and z
    - x, hydrogen abundance
    - y, helium abundance
    - z, metal abundance
    
    Default flow is pure hydrogen, x=1,y&z=0.
    """
    return ((2.0*x)+(0.75*y)+(0.5*z))**-1

def temperature(rhodata,pgdata,mu,scaletomks=True):
    """
    Calculates temperature from rho data and pgdata, can be fed values, or entire numpy arrays.
    """
    k_b=1.38065e-23
    m_h=1.6727e-27
#    if scaletomks==True:
#        rhodata*=1000
#        pgdata*=0.1
    tempdata=(pgdata/rhodata)*((mu*m_h)/k_b)*(0.1/1000)
    return tempdata


def ext(x,y):
    xstep=np.max(np.diff(x))/2
    ystep=np.max(np.diff(y))
    ext=[min(x)-xstep,max(x)+xstep,min(y)-ystep,max(y)+ystep]
    return ext

def contourplot(x,y,array,
                nlevs=10,
                colour="black"):
    """
    Plots contours, interpolating grid data to higher resolution
    """
    import scipy.interpolate as inter
    import scipy.ndimage as ndimage
    n=1000
    lvl=np.logspace(np.floor(np.log10(min(array))),
                    np.floor(np.log10(max(array))),
                    nlevs)
    xi=np.linspace(min(x),max(x),n)
    yi=np.linspace(min(x),max(x),n)
    zi=inter.griddata((x,y),array,(xi[None,:], yi[:,None]), method='cubic')
    zif = ndimage.gaussian_filter(zi, sigma=5, order=0)
    cs=plt.contour(xi,yi,zif,origin="lower",
                levels=lvl,
                colors=colour,
                linewidths=0.5)
    plt.clabel(cs,fmt="%.3g",
               fontsize=4)
    
def savefigure(filename,
               dpi=600,bbox_inches="tight"):
    """
    Does what it says on the tin, just a time-saver more than anything
    Normally saves at 600 dpi for png files
    Ideally save as a pdf, unless it's an image
    """
    plt.savefig(filename,
                dpi=dpi,
                bbox_inches=bbox_inches)

#def saveplot(plt)

#def rplot():
#    #Export data to R plot3D, for isosurfaces
    