#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 16:08:42 2018

@author: Joseph Eatson

This code sanitises data from mg .xq exports since graphics functions plot data
opposite to how it is performed in arwen. This programme also strips headers
from the data, which is necessary to read into the C code.
"""
#User variables

multicore=False #Enable multicore processing, reduces time to compute, but increases memory requirements, and unstable as a batch process
ncores=2 #Number of threads to use in multiprocessing, drastically increases memory requirements!
skipheader=10 #Declare number of lines to skip for header


import numpy as np
import multiprocessing
import sys,time
filename=sys.argv[1]
#filename="3dray"
# All files must be present, otherwise the convresion will not work
rho=filename+"-rho.xq"
pg=filename+"-pg.xq"
col=filename+"-col.xq"
u1=filename+"-u1.xq"
u2=filename+"-u2.xq"
u3=filename+"-u3.xq"
pool1=[rho,pg,col,u1,u2]
pool2=["x","y"]

def readheader(filename):
        f=open(filename)
        l1=f.next().strip("\n").split(" ")
        ndim=int(l1[0])
        xres=int(l1[1])
        yres=int(l1[2])
        if ndim==3:zres=int(l1[3])
        else: zres=1
        #zres=int(l1[3])
        f.close()
        return ndim,xres,yres,zres
def readfile(inname):
    outname=inname+".temp"
    if ndim==2:
        eigen="ij->ji"
        res=(xres,yres)
    if ndim==3:
        eigen="ijk->kji"
        res=(xres,yres,zres)
#    try:    
#    array2=(np.genfromtxt(inname,delimiter=" ", skip_header=skipheader)[:,2]).reshape((xres,yres)).swapaxes(0,1).reshape(xres,yres)
#    except:
#        raise Exception("Could not find file "+inname)
    print "Reading "+inname+"..."
    t=time.time()
    array=(np.genfromtxt(inname,delimiter=" ", skip_header=skipheader)[:,ndim]).reshape(res)
    print "Finished! ({:.3f} sec)".format(time.time()-t)
    #print "Processing "+inname+"..."
    #t=time.time()
    #array=np.einsum(eigen,array)
    array=array.ravel()
    #print "Finished! ({:.3f} sec)".format(time.time()-t)
    writefile(inname,array,outname)
    del array
def writefile(inname,array,outname):
    print "Writing "+outname+"..."
    t=time.time()
    np.savetxt(outname,array,fmt="%.6E",delimiter="\n")
    print "Finished! ({:.3f} sec)".format(time.time()-t)
def readxyz(axis):
    if axis=="x": col=0
    if axis=="y": col=1
    if axis=="z": col=2
    try:
        array=(np.genfromtxt(rho,delimiter=" ",skip_header=skipheader))[:,col]
    except:
        raise Exception("Could not find file "+ rho)
    outname=filename+"-"+axis+".xq.temp"
    writefile(filename,array,outname)
    del array
ndim,xres,yres,zres=readheader(rho)
if multicore==True:
    if __name__ == '__main__':
        pool=multiprocessing.Pool(processes=ncores)
        pool.map(readfile, pool1)
        pool.map(readxyz, pool2)
        pool.close()
        pool.join()
if multicore==False:
    readfile(rho)
    readfile(pg)
    readfile(col)
    readfile(u1)
    readfile(u2)
    if ndim==3:readfile(u3)
    readxyz("x")
    readxyz("y")
    if ndim==3:readxyz("z")
sys.exit()
