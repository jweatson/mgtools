library(data.table)
options(digits = 6, scipen = -999)
groupname="raytrace-hi"
skiplines=10
datatypes=c("rho",
            "pg",
            "col",
            "u1",
            "u2")
readheader=function(x,y){
  as.numeric(header[[x]][y])
}
transform=function(filename){
  scale=rscale
  data=fread(input = filename,
             sep=" ")
  #mat=matrix(data[,3],xres,yres)
  #mat=t(mat)
  #mat=matrix(mat,xres*yres)
  #mat=list(mat)
  mat=array(unlist(data[,3]),c(xres,yres))
  mat=list(mat)
  return(mat)
  
  #return(data)
}
writedata=function(data,filename){
  fwrite(x = data,
         file = paste(filename,".temp",sep=""),
         col.names = FALSE)
}
#Header parsing
header=strsplit(readLines(paste(groupname,"-rho.xq",sep=""),n=skiplines)," ")
xres=readheader(1,2)
yres=readheader(1,3)
rscale=readheader(3,2)
uscale=readheader(4,2)
rhoscale=readheader(5,2)
pgscale=readheader(6,2)
tscale=readheader(7,2)
time=readheader(8,2)

writedata(transform(filename = paste(groupname,"-rho.xq",sep="")),
          paste(groupname,"-rho.xq",sep=""))
# writedata(transform(filename = paste(groupname,"-pg.xq",sep="")),
#           paste(groupname,"-pg.xq",sep=""))
# writedata(transform(filename = paste(groupname,"-col.xq",sep="")),
#           paste(groupname,"-col.xq",sep=""))
# writedata(transform(filename = paste(groupname,"-u1.xq",sep="")),
#           paste(groupname,"-u1.xq",sep=""))
# writedata(transform(filename = paste(groupname,"-u2.xq",sep="")),
#           paste(groupname,"-u2.xq",sep=""))

#data=transform(filename = paste(groupname,"-rho.xq",sep=""))
gc()


