#Simple sanity check for folder data
#setwd("~/Documents/rayrace/outputs")
library(FITSio)
files=list.files(pattern = "*.fits",full.names = T,recursive = FALSE)
floor=1e-300 #Optionally defines minimum value, since 64 bit number can be extremely small
for (n in files){
  data=readFITS(file = n)
  #data$imDat[data$imDat==0]=floor
  writeFITSim(data$imDat,file = paste(n,".temp",sep=""),header = data$header)
}
gc()
