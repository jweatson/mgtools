#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <ctime>
#include "fitsio.h"
#include "longnam.h"

/*
This program converts .xq output files from mg to .fits files for use with ray-tracing code
This conversion also provides a significant space saving over using .xq ASCII files
Additional compression stage after writing to ensure minimal filesize
Requires gzip and head programmes, which are typically installed on Linux and MacOS systems

This programme is written for RZP COORDINATES, and data requires transpositon first using transform.py
*/

// CONSTANTS, edited by user
std::string nstrip = "11"; // Strip n lines from files, for removing header, this is only ever used as a string, typically "4"
std::string problem = "CWB"; // State problem
float RSCALE = 1e12; // Distance scale
float TSCALE = 1e4; // Time scale
double T = 4e1; // Time elapsed
float RHOSCALE = 1e-20; // Density scale
float PGSCALE = 1e-4; // Pressure scale
float USCALE = 1e8; // Velocity scale

// CWB CONSTANTS, ignored if problem isn't CWB
double KMS = 1e5;
double MDOT1 = 30;
double MDOT2 = 30;
double VINF1 = 2.5e8/KMS;
double VINF2 = 2.5e8/KMS;
double ETA = 0;
double DSEP = 4*RSCALE;
double XPOS_1 = 0;
double YPOS_1 = 0;
double XPOS_2 = 0;
double YPOS_2 = 4*RSCALE;
double X1 = 0.5; // Hydrogen abundance of star 1
double X2 = 0.5; // Hydrogen abundance of star 2
double Y1 = 0.5; // Helium abundance of star 1
double Y2 = 0.5; // Helium abundance of star 2

// FUNCTIONS
bool fexists(const std::string& filename) {
  std::ifstream ifile(filename.c_str());
  return (bool)ifile;
}

bool ftest(const std::string& filetype, const std::string& filename) {
  bool fileexists;
  if (fexists(filename) == true) {
    std::cout << filetype << ": " << filename << '\n';
    fileexists = true;
  } else {
    std::cout << filetype << ": Not Found!" << '\n';
    fileexists = false;
  }
  return fileexists;
}

std::string fstrip(const std::string& filename) {
  // 06-02-2018 09:31 This function utilises bash/csh commands to strip headers from files, its slightly quicker this way
  // This does increase processing time for each file, but this is a faster method than using my own algorithm
  bool fileexists = fexists(filename);
  if (fileexists == false) {
    std::cerr << "Could not find file " << filename << ", exiting!" << '\n';
    exit(1);
  }
  std::string tempname = filename+".temp";
  system(("tail -n +" + nstrip + " " + filename + " > " + tempname).c_str()); //Creates new, temporary file with header stripped
  return tempname;
}

int findlength(std::string filename) {
  int nlines = 0;
    std::string line;
    std::ifstream myfile(filename);
    while (std::getline(myfile, line))
        ++nlines;
  return nlines;
}

float * readfiles(std::string filename, std::string choice, float scale, int nlines) {
  // 31-01-2018 22:56 Add skiplines, to eliminate the need for reformatting data
  // 31-01-2018 22:56 Add scales, to rescale data while it is being fed into the arrays, since disk access is the bottleneck you might as well do something with it
  //23-01-2018 18:31 Currently this reads all values to 3 dummy arrays, and returns one that is required
  //This would be better served by only reading in a specific line, would save on memory usage
  float dummy;
  float *qtemp = new float[nlines];
  std::ifstream infile;
  unsigned int num = 0;
  infile.open(filename);
  if(infile.fail()) {
      std::cerr << "Error, could not open file!" << "\n";
      exit(1);
  }
  // if (choice == "x") {
  //   while(!infile.eof()) {
  //      infile >> qtemp[num];
  //      infile >> dummy;
  //      infile >> dummy;
  //      qtemp[num]=qtemp[num]*scale;
  //      ++num;
  //   }
  // }
  // if (choice == "y") {
  //   while(!infile.eof()) {
  //      infile >> dummy;
  //      infile >> qtemp[num];
  //      infile >> dummy;
  //      qtemp[num]=qtemp[num]*scale;
  //      ++num;
  //   }
  // }
  // if (choice == "rho" || "pg" || "u" || "colour") {
  //   while(!infile.eof()) {
  //      infile >> dummy;
  //      infile >> dummy;
  //      infile >> qtemp[num];
  //      qtemp[num]=qtemp[num]*scale;
  //      ++num;
  //   }
    while (!infile.eof()) {
      infile >> qtemp[num];
      qtemp[num]=qtemp[num]*scale;
      ++num;
    }
  std::cout << "Imported " << choice << "!" << '\n';
  infile.close();
  return qtemp;
}

int main(int argc, char const *argv[]) {
  // DECLARATIONS
  unsigned int nlines; // Number of lines found in data
  int ndim; // Number of dimensions
  float rscale, tscale, rhoscale, pgscale, uscale; // Local versions of global variables, in case there is a config file detected
  std::string filename; // General filename, specific filesnames for data include -rho, -pg etc.
  bool configexists,densityexists,pressureexists,colourexists,u1exists,u2exists,u3exists; // Logic for confirming existence of files
  long imax,jmax,kmax; //Array sizes, determined manually or from assuming square/cubic
  float *x, *y, *z, *rho, *col, *pg, *u1, *u2, *u3; // Arrays for storing requisite data
  float max_x, max_y, max_z, min_x, min_y, min_z; // Max x,y,z values
  float diff_x, diff_y, diff_z; // Per-cell differential for x,y,z
  // FITS DECLARATIONS
  int status = 0; //FITS status value
  fitsfile *fitsptr;
  long fpixel = 1;
  long naxis;
  long npixels;
  // Dummy values, to fool raycode into thinking it is running ARWEN exports
  int nprin1d = 1000000000;
  int nprin2d = 1000000000;
  int nprin3d = 1000000000;
  int nfile1d = 0;
  int nfile2d = 400;
  int nfile3d = 0;
  int ncycp1d = 34525;
  int ncycp2d = 0;
  int ncycp3d = 34525;
  double t = T;
  double dt = t/200;
  double timep1d = 100000000000;
  double timep2d = 100000000000;
  double timep3d = 100000000000;
  double tprin1d = 1e99;
  double tprin2d = 15778450000;
  double tprin3d = 1e99;
  float avgmass1 = 1.0e-24; // Average mass, needed for X-ray calculations
  float avgmass2 = 1.0e-24;
  // Values for CWB case, need to not be hard-coded
  double mdot1 = MDOT1;
  double mdot2 = MDOT2;
  double vinf1 = VINF1;
  double vinf2 = VINF2;
  double eta = ETA;
  double dsep = DSEP;
  double xpos_1 = XPOS_1;
  double ypos_1 = YPOS_1;
  double xpos_2 = XPOS_2;
  double ypos_2 = YPOS_2;
  double x1 = X1; // Hydrogen abundance of star 1
  double x2 = X2; // Hydrogen abundance of star 2
  double y1 = Y1; // Helium abundance of star 1
  double y2 = Y2; // Helium abundance of star 2

  // CODE
  // Start the clock, because all good programmes require you to know how long they took
  clock_t begin = clock();

  // Print vanity logo
  std::cout << "***********************************" << '\n';
  std::cout << "*** X2F version 1.1, \"THRILLHO\" ***" << '\n';
  std::cout << "*** RZP 2D TO FITS CONVERTER    ***" << '\n';
  std::cout << "***********************************" << '\n';

  // Check number of parameters
  if ( argc < 3 ) {
    std::cerr << "Please include the general filename, and ndim as arguments!" << '\n';
    std::cerr << "Exiting!" << '\n';
    return 1;
  }
  if ( argc < 4 ) {
    std::cerr << "No array dimensions found, assuming square/cubic" << '\n';
  }
  // Arguments
  filename = argv[1];
  ndim = atoi(argv[2]);

  if (ndim > 3) {
    std::cerr << "Welcome " << '\n';
    std::cerr << "Enter an ndim between 1 and 3." << '\n';
    return 1;
  } else if (ndim < 1) {
    std::cerr << "What a terrifying, " << ndim << "-dimensional world you live in" << '\n';
    return 1;
  }
  // Scales, initially to be hardcoded, but test for a config file
  // Converts to local scale to allow for config files
  rscale = RSCALE;
  tscale = TSCALE;
  //t = T;
  rhoscale = RHOSCALE;
  pgscale = PGSCALE;
  uscale = USCALE;
  // Filenames
  std::string configname = filename + "-config.txt";
  std::string rhoname = filename + "-rho.xq.temp";
  std::string pgname = filename + "-pg.xq.temp";
  std::string colname = filename + "-col.xq.temp";
  std::string u1name = filename + "-u1.xq.temp";
  std::string u2name = filename + "-u2.xq.temp";
  std::string u3name = filename + "-u3.xq.temp";
  std::string outname = filename + ".fits";
  std::string gzname = filename + ".fits.gz";
  std::string xname = filename + "-x.xq.temp";
  std::string yname = filename + "-y.xq.temp";
  std::string zname = filename + "-z.xq.temp";

  //Arguments found, assuming non-square
  //29-01-2018 18:54 More elegant solution required, this segfaults
  // if ( argc == 4 ) {
  //   imax = atoi(argv[3]);
  // }
  // if (argc == 5 ) {
  //   imax = atoi(argv[3]);
  //   jmax = atoi(argv[3]);
  // }
  // if (argc == 6 ) {
  //   imax = atoi(argv[3]);
  //   jmax = atoi(argv[3]);
  //   kmax = atoi(argv[3]);
  // }

  //Print filenames and number of dimensions
  std::cout << "---------------" << '\n';
  std::cout << "Filenames" << '\n';
  std::cout << "---------------" << '\n';
  configexists=ftest("Config", configname);
  densityexists=ftest("Density", rhoname);
  pressureexists=ftest("Pressure", pgname);
  colourexists=ftest("Colour", colname);
  u1exists=ftest("X Velocity", u1name);
  u2exists=ftest("Y Velocity", u2name);
  u3exists=ftest("Z Velocity", u3name);
  std::cout << "Output: " << outname << '\n';
  std::cout << "---------------" << '\n';

  // Strip headers from files, does take some time with large files
  // rhoname = fstrip(rhoname);
  // pgname = fstrip(pgname);
  // colname = fstrip(colname);
  // u1name = fstrip(u1name);
  // u2name = fstrip(u2name);
  // if (ndim==3){
  //   u3name = fstrip(u3name);
  // }

  // Find array shape and number of lines
  nlines = findlength(rhoname);
  // Assume square
  // imax = int(sqrt(float(nlines)));
  // jmax = int(sqrt(float(nlines)));
  imax = std::stoi(argv[3]);
  jmax = std::stoi(argv[4]);
  kmax = std::stoi(argv[5]);

  // Read array details and scales
  std::cout << "Scales" << '\n';
  std::cout << "---------------" << '\n';
  std::cout << "Ndim:       " << ndim << '\n';
  if (ndim==2) {
    printf("Resoluton:  (%d,%d)\n", jmax,imax);
  }
  if (ndim==3) {
    printf("Resoluton:  (%d,%d,%d)\n", kmax,jmax,imax);
  }
  std::cout << "rscale: " << rscale << " cm\n";
  std::cout << "uscale: " << uscale << " cm/s\n";
  std::cout << "rhocale: " << rhoscale << " g/cm^3\n";
  std::cout << "pgscale: " << pgscale << " g/cms^2\n";
  std::cout << "tscale: " << tscale << " sec\n";
  std::cout << "time: " << t*tscale << " sec\n";
  std::cout << "---------------" << "\n";

  // Replace with temporary files, reuses variables
  // 06-02-2018 09:38 Non-essential, but best to refactor at some point
  // 06-02-2018 09:39 Tests for files need to be included before running
  // Test if all files are correctly accounted for
  if ((densityexists || pressureexists || u1exists || u2exists) == false) {
    std::cerr << "Error: Please provide all .xq files for conversion!" << '\n';
    return 1;
  } else {
    std::cout << "Working..." << '\n';
  }
  if ((u3exists == false) && (ndim == 3)) {
    std::cerr << "3-D problem, please include Z velocity file (u3)" << '\n';
    return 1;
  }

  int narrays = ndim+5;
  x = readfiles(xname, "x", rscale, nlines);
  y = readfiles(yname, "y", rscale, nlines);
  if ( ndim == 3 ) {
    z = readfiles(zname, "z", rscale, nlines);
  }
  rho = readfiles(rhoname, "rho", rhoscale, nlines);
  col = readfiles(colname, "colour", 1.0, nlines);
  pg = readfiles(pgname, "pg", pgscale, nlines);
  u1 = readfiles(u1name, "u", uscale, nlines);
  u2 = readfiles(u2name, "u", uscale, nlines);
  if ( ndim==3 ) {
    u3 = readfiles(u3name, "u", uscale, nlines);
  }

  printf("Finished importing %d datapoints!\n", nlines*narrays);
  //Remap x,y,z coordinates to linear arrays
  //29-01-2018 16:18 Array declaration needs a cleanup
  //29-01-2018 17:18 Actually most of this needs a cleanup
  float xrange[imax+1], xdiff[imax+1];
  float yrange[jmax+1], ydiff[jmax+1];
  float zrange[kmax+1], zdiff[kmax+1];
  min_x = x[0];
  max_x = x[1];
  diff_x = x[1]-x[0];
  std::fill_n(xdiff, imax+1, diff_x/2); //Fill x diff array
  //29-01-2018 17:35 Make this more elegant
  for (size_t n = 0; n < imax+1; n++) {
    xrange[n] = (x[0]-(diff_x/2))+(n*diff_x);
  }
  if ( ndim == 2 ) {
    min_y = y[0];
    max_y = y[nlines-1];
    diff_y = y[jmax]-y[0];
    std::fill_n(ydiff, jmax+1, diff_y/2); //Fill y diff array
    for (size_t n = 0; n < jmax+1; n++) {
      yrange[n] = (y[0]-(diff_y/2))+(n*diff_y);
    }
  }
  if ( ndim == 3 ) {
    min_y = y[0];
    max_y = y[nlines-1];
    diff_y = y[jmax]-y[0];
    min_z = z[0];
    max_z = z[nlines-1];
    diff_z = z[kmax*kmax]-z[0];
    std::fill_n(ydiff, jmax+1, diff_y/2); //Fill y diff array
    std::fill_n(zdiff, kmax+1, diff_z/2); //Fill z diff array
    for (size_t n = 0; n < jmax+1; n++) {
      yrange[n] = (y[0]-(diff_y/2))+(n*diff_y);
    }
    for (size_t n = 0; n < kmax+1; n++) {
      zrange[n] = (z[0]-(diff_z/2))+(n*diff_z);
    }
  }
  if (remove(outname.c_str( )) !=0) {
    std::cout << "Writing file " << outname << '\n';
  }


  fits_create_file(&fitsptr, outname.c_str( ), &status); //create file, needs to be rewritten!
  int bitpix=FLOAT_IMG;
  long naxes[ndim];
  if (ndim == 2){
    naxis = 2;
    naxes[0] = imax;
    naxes[1] = jmax;
  }
  if (ndim == 3){
    naxis = 3;
    naxes[0] = imax;
    naxes[1] = jmax;
    naxes[2] = kmax;
  } //This is pretty sloppy, rewrite this
  npixels = imax*jmax*kmax;
  fits_create_img(fitsptr, FLOAT_IMG, naxis, naxes, &status);
  //fits_update_key(fitsptr,TSTRING,"problem","cwb","problem",&status);
  //
  //
  // // 30-01-2018 12:34
  // // Integer values
  //fits_update_key(&fitsptr,TINT,"ncycle",ncycle,"ncycle",&status);
  // fits_update_key(&fitsptr,TINT,"nfile1d",nfile1d,"nfile1d",&status);
  // fits_update_key(&fitsptr,TINT,"nfile2d",nfile2d,"nfile2d",&status);
  // fits_update_key(&fitsptr,TINT,"nfile3d",nfile3d,"nfile3d",&status);
  char extnamerho[8];
  strcpy(extnamerho,"density");
  fits_update_key(fitsptr,TSTRING,"vh_data",extnamerho,"Extension name",&status);
  fits_update_key(fitsptr,TINT,"nprin1d",&nprin1d,"nprin1d",&status);
  fits_update_key(fitsptr,TINT,"nprin2d",&nprin2d,"nprin2d",&status);
  fits_update_key(fitsptr,TINT,"nprin3d",&nprin3d,"nprin3d",&status);
  fits_update_key(fitsptr,TINT,"ncycp1d",&ncycp1d,"ncycp1d",&status);
  fits_update_key(fitsptr,TINT,"ncycp2d",&ncycp2d,"ncycp2d",&status);
  fits_update_key(fitsptr,TINT,"ncycp3d",&ncycp3d,"ncycp3d",&status);
  // // Long integer values
  fits_update_key(fitsptr,TDOUBLE,"time",&t,"time",&status);
  fits_update_key(fitsptr,TDOUBLE,"dt",&dt,"dt",&status);
  fits_update_key(fitsptr,TDOUBLE,"timep1d",&timep1d,"timep1d",&status);
  fits_update_key(fitsptr,TDOUBLE,"timep2d",&timep2d,"timep2d",&status);
  fits_update_key(fitsptr,TDOUBLE,"timep3d",&timep3d,"timep3d",&status);
  fits_update_key(fitsptr,TDOUBLE,"tprin1d",&tprin1d,"tprin1d",&status);
  fits_update_key(fitsptr,TDOUBLE,"tprin2d",&tprin2d,"tprin2d",&status);
  fits_update_key(fitsptr,TDOUBLE,"tprin3d",&tprin3d,"tprin3d",&status);
  fits_update_key(fitsptr,TFLOAT,"avgmass1",&avgmass1,"average mass 1",&status);
  fits_update_key(fitsptr,TFLOAT,"avgmass2",&avgmass2,"average mass 2",&status);
  //
  // // Values for orbital proponents, need to be added to config file
  fits_update_key(fitsptr,TDOUBLE,"Mdot_1",&mdot1,"(Msol/yr)",&status);
  fits_update_key(fitsptr,TDOUBLE,"Mdot_2",&mdot2,"(Msol/yr)",&status);
  fits_update_key(fitsptr,TDOUBLE,"vinf_1",&vinf1,"(km/s)",&status);
  fits_update_key(fitsptr,TDOUBLE,"vinf_2",&vinf2,"(km/s)",&status);
  fits_update_key(fitsptr,TDOUBLE,"eta",&eta,"(no units)",&status);
  fits_update_key(fitsptr,TDOUBLE,"dsep",&dsep,"(cm)",&status);
  fits_update_key(fitsptr,TDOUBLE,"xpos_1",&xpos_1,"(cm)",&status);
  fits_update_key(fitsptr,TDOUBLE,"ypos_1",&ypos_2,"(cm)",&status);
  //fits_update_key(&fitsptr,TDOUBLE,"zpos1",&stzpos[0],"(cm)",&status);
  fits_update_key(fitsptr,TDOUBLE,"xpos_2",&xpos_1,"(cm)",&status);
  fits_update_key(fitsptr,TDOUBLE,"ypos_2",&ypos_2,"(cm)",&status);
  fits_update_key(fitsptr,TDOUBLE,"X1",&x1,"X1",&status);
  fits_update_key(fitsptr,TDOUBLE,"X2",&x2,"X2",&status);
  fits_update_key(fitsptr,TDOUBLE,"Y1",&y1,"Y1",&status);
  fits_update_key(fitsptr,TDOUBLE,"Y2",&y2,"Y2",&status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixels, rho, &status);
  std::cout << "Written rho!" << '\n';
  // if (status != 0){
  //   std::cerr << "Error writing CWB keywords." << '\n';
  //   return 0;
  // }

  //PRESSURE
  fits_create_img(fitsptr, FLOAT_IMG, naxis, naxes, &status);
  // 30-01-2018 16:52 I really need to convert this to a function because this is an absolute mess, but it works!
  // 30-01-2018 17:17 This is easy to conver to a function
  char extnamepr[9];
  strcpy(extnamepr,"pressure");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamepr, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixels, pg, &status);
  std::cout << "Written pg!" << '\n';

  //X VELOCITY
  fits_create_img(fitsptr, FLOAT_IMG, naxis, naxes, &status);
  char extnamevx[3];
  strcpy(extnamevx,"vx");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamevx, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixels, u1, &status);
  std::cout << "Written u1!" << '\n';

  //Y VELOCITY
  fits_create_img(fitsptr, FLOAT_IMG, naxis, naxes, &status);
  char extnamevy[3];
  strcpy(extnamevy,"vy");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamevy, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixels, u2, &status);
  std::cout << "Written u2!" << '\n';

  if ( ndim == 3){
    fits_create_img(fitsptr, FLOAT_IMG, naxis, naxes, &status);
    char extnamevy[3];
    strcpy(extnamevy,"vz");
    fits_update_key(fitsptr, TSTRING, "vh_data", extnamevy, "Extension name", &status);
    fits_write_img(fitsptr, TFLOAT, fpixel, npixels, u3, &status);
    std::cout << "Written u3!" << '\n';
  }

  fits_create_img(fitsptr, FLOAT_IMG, naxis, naxes, &status);
  char extnamecol[7];
  strcpy(extnamecol,"colour");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamecol, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixels, col, &status);
  std::cout << "Written colour!" << '\n';

  int xyznaxis = 2;
  long xnaxes[2];
  long ynaxes[2];
  long znaxes[2];
  xnaxes[0] = imax+1;
  xnaxes[1] = 1;
  ynaxes[0] = jmax+1;
  ynaxes[1] = 1;
  znaxes[0] = kmax+1;
  znaxes[1] = 1;

  long npixelslinearx = (imax+1);
  long npixelslineary = (jmax+1);
  long npixelslinearz = (kmax+1);
  //
  //X axis
  fits_create_img(fitsptr, FLOAT_IMG, xyznaxis, xnaxes, &status);
  char extnamezxa[4];
  strcpy(extnamezxa,"zxa");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamezxa, "Extension name", &status);
  std::cout << npixelslinearx << '\n';
  fits_write_img(fitsptr, TFLOAT, fpixel, npixelslinearx, xrange, &status);
  std::cout << "Written x!" << '\n';

  //Y axis
  fits_create_img(fitsptr, FLOAT_IMG, xyznaxis, ynaxes, &status);
  char extnamezya[4];
  strcpy(extnamezya,"zya");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamezya, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixelslineary, yrange, &status);
  std::cout << "Written y!" << '\n';

  char extnamezza[4];
  strcpy(extnamezza,"zza");
  if ( ndim == 3 ){
    fits_create_img(fitsptr, FLOAT_IMG, xyznaxis, ynaxes, &status);
    fits_update_key(fitsptr, TSTRING, "vh_data", extnamezza, "Extension name", &status);
    fits_write_img(fitsptr, TFLOAT, fpixel, npixelslinearz, zrange, &status);
    std::cout << "Written z!" << '\n';
  }

  //X diff
  fits_create_img(fitsptr, FLOAT_IMG, xyznaxis, xnaxes, &status);
  char extnamezdx[4];
  strcpy(extnamezdx,"zdx");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamezdx, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixelslinearx, xdiff, &status);
  std::cout << "Written dx!" << '\n';

  //Y diff
  fits_create_img(fitsptr, FLOAT_IMG, xyznaxis, ynaxes, &status);
  char extnamezdy[4];
  strcpy(extnamezdy,"zdy");
  fits_update_key(fitsptr, TSTRING, "vh_data", extnamezdy, "Extension name", &status);
  fits_write_img(fitsptr, TFLOAT, fpixel, npixelslineary, ydiff, &status);
  std::cout << "Written dy!" << '\n';

  //Z diff
  char extnamezdz[4];
  strcpy(extnamezdz,"zdz");
  if ( ndim == 3 ){
    fits_create_img(fitsptr, FLOAT_IMG, xyznaxis, ynaxes, &status);
    fits_update_key(fitsptr, TSTRING, "vh_data", extnamezdz, "Extension name", &status);
    fits_write_img(fitsptr, TFLOAT, fpixel, npixelslineary, zdiff, &status);
    std::cout << "Written dz!" << '\n';
  }

  // Report errors and confirm file write
  fits_report_error(stderr, status);
  fits_close_file(fitsptr, &status);

  // std::cout << "Compressing..." << '\n';
  // if (remove(gzname.c_str( )) !=0) {
  //   std::cout << "Writing compressed file " << gzname << '\n';
  // }
  // system(("gzip " + outname).c_str());

  // Remove temp files, general garbage collection
  std::cout << "Cleaning up!" << '\n';
  //system("rm *.temp");
  delete []rho;
  delete []pg;
  delete []col;
  delete []u1;
  delete []u2;
  if (ndim == 3) {
    delete []u3;
  }

  clock_t end = clock();
  std::cout << "Done! Took " << float( end-begin ) / CLOCKS_PER_SEC << " sec\n";
  return 0;
}
